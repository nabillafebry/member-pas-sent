# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/member-pas-sent.jar /member-pas-sent.jar
# run application with this command line[
CMD ["java", "-jar", "/member-pas-sent.jar"]
