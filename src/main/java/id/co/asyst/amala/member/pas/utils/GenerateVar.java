package id.co.asyst.amala.member.pas.utils;

import id.co.asyst.commons.core.utils.DateUtils;
import id.co.asyst.commons.core.utils.GeneratorUtils;
import org.apache.camel.Exchange;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class GenerateVar {
    public static String generate_messageID() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(System.currentTimeMillis());
        byte[] messageIDValue = new byte[16];
        random.nextBytes(messageIDValue);
        return Base64.encode(messageIDValue);
    }

    public static String generate_nonce() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(System.currentTimeMillis());
        byte[] nonceValue = new byte[16];
        random.nextBytes(nonceValue);
        return Base64.encode(nonceValue);
    }

    public static String generate_created(Exchange exchange) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String syncdate = sdf.format(cal.getTime());
        exchange.setProperty("created", syncdate);

        format.setTimeZone(TimeZone.getTimeZone("Zulu"));
        return format.format(cal.getTime());
    }

    public static byte[] password_encrypt(String algorithm, String clearPassword) throws NoSuchAlgorithmException {
        MessageDigest sha = MessageDigest.getInstance(algorithm);
        sha.reset();
        String password = clearPassword;

        byte[] b1 = password.getBytes(StandardCharsets.UTF_8);
        sha.update(b1);

        return sha.digest();
    }

    public static String password_digest(byte[] password, String nonce, String created) throws WSSecurityException, NoSuchAlgorithmException {
        String passwdDigest = null;
        byte[] b1 = nonce != null ? Base64.decode(nonce) : new byte[0];
        byte[] b2 = created != null ? created.getBytes(StandardCharsets.UTF_8) : new byte[0];
        byte[] b3 = password;
        byte[] b4 = new byte[b1.length + b2.length + b3.length];
        int offset = 0;
        System.arraycopy(b1, 0, b4, offset, b1.length);
        offset += b1.length;
        System.arraycopy(b2, 0, b4, offset, b2.length);
        offset += b2.length;
        System.arraycopy(b3, 0, b4, offset, b3.length);
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        sha.reset();
        sha.update(b4);
        passwdDigest = Base64.encode(sha.digest());
        return passwdDigest;
    }

    public static String generate_uniqueID() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(System.currentTimeMillis());
        byte[] uniqueIDValue = new byte[16];
        random.nextBytes(uniqueIDValue);
        return Base64.encode(uniqueIDValue);
    }

    public void generateSyncID(Exchange exchange) {
        String idmember = "";
        String idmileage = "";
        idmember = GeneratorUtils.GenerateId("MEMBER", DateUtils.now(), 6);
        idmileage = GeneratorUtils.GenerateId("MILEAGE", DateUtils.now(), 6);

        exchange.setProperty("idmember", idmember);
        exchange.setProperty("idmileage", idmileage);
    }
}
