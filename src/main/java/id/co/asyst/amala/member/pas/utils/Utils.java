package id.co.asyst.amala.member.pas.utils;

import id.co.asyst.commons.core.utils.DateUtils;
import id.co.asyst.commons.core.utils.GeneratorUtils;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.camel.Exchange;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.ws.security.WSSecurityException;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static Logger log = LogManager.getLogger(Utils.class);

    public static final String SUCCESS = "<Success\\/><UniqueID Type=\"21\" ID=\"(\\w+)\" ID_Context=\"CSX\"\\/>";
    public static final String ERROR = "<Error Type=\"21\" ShortText=\"([\\w. ]+)\" Code=\"([\\w.]+)\"\\>";
    public static final String SUCCESSUPDATE = "<Success\\/><UniqueID Type=\"21\" Instance=\"(\\w+)\" ID=\"(\\w+)\" ID_Context=\"CSX\"\\/>";


    public void getCardnumber(Exchange exchange) throws ParseException {
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("res");
        List<Map<String, Object>> cards = (List<Map<String, Object>>) memberProfile.get("membercards");
        log.info("CARDS:: " + cards);
        String cardnumber = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.HOUR_OF_DAY, 7);

        String eff = "";
        String exp = "";
        Date effdate = new Date();
        Date expdate = new Date();

        log.info("TODAY:: " + formatter.format(c.getTime()));
        for (Map<String, Object> card : cards) {
            if (card.get("status").equals("ACTIVE")) {
                eff = (String) card.get("effectivedate");
                effdate = formatter.parse(eff);

                if (card.get("expireddate") != null) {
                    exp = (String) card.get("expireddate");
                    expdate = formatter.parse(exp);
                }

                if ((eff.equals(formatter.format(c.getTime())) || effdate.before(formatter.parse(formatter.format(c.getTime()))))) {
                    if (exp != "") {
                        cardnumber = (String) card.get("cardnumber");
                        log.info("CARDNUMBER EFFDATE:: " + eff + " AND EXPDATE IS NULL");
                    } else {
                        if (exp.equals(formatter.format(c.getTime())) || expdate.after(formatter.parse(formatter.format(c.getTime())))) {
                            cardnumber = (String) card.get("cardnumber");

                            log.info("CARDNUMBER EFFDATE:: " + eff + " AND EXPDATE:: " + exp);
                        }
                    }
                }
            }
        }
        exchange.setProperty("cardnumber", cardnumber);
    }

    public void checkTier(Exchange exchange) throws ParseException {
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("res");
        log.info("RES::" + memberProfile);
        List<Map<String, Object>> tiers = (List<Map<String, Object>>) memberProfile.get("membertiers");
        String tierid = "";
        Map<String, Object> futuretier = new HashMap<>();
        List<Map<String, Object>> listfuturetier = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.HOUR_OF_DAY, 7);

        String start = "";
        String end = "";
        Date startdate = new Date();
        Date enddate = new Date();

        for (Map<String, Object> tier : tiers) {
            if (tier.get("active").equals(true)) {
                start = (String) tier.get("startdate");
                startdate = formatter.parse(start);

                if (tier.get("enddate") != null) {
                    end = (String) tier.get("enddate");
                    enddate = formatter.parse(end);
                }


                if (startdate.after(formatter.parse(formatter.format(c.getTime())))) {
                    futuretier.put("tierid", tier.get("tierid"));
                    futuretier.put("startdate", tier.get("startdate"));
                    listfuturetier.add(futuretier);
                } else {
                    if (end.equals("")) {
                        log.info("ENDDATE IS NULL");
                        tierid = (String) tier.get("tierid");
                    } else {
                        if (enddate.after(c.getTime()) || end.equals(formatter.format(c.getTime()))) {
                            tierid = (String) tier.get("tierid");

                            log.info("TIERID:: " + tierid);
                        } else {
                            log.info("TIERID WITH START " + start + " AND END " + end);
                        }
                    }
                }

            }
        }

        exchange.setProperty("listfuturetier", listfuturetier);
        exchange.setProperty("tierid", tierid);
    }

    public void generateFutureTierID(Exchange exchange) {
        String futuretierid = GeneratorUtils.GenerateId("", DateUtils.now(), 5);

        exchange.setProperty("futuretierid", futuretierid);
    }

    /* set body soapui to request*/
    @SuppressWarnings("unchecked")
    public void setbodysoapCreate(Exchange exchange) throws IOException, WSSecurityException, NoSuchAlgorithmException {
        String retrocredit = "";
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("res");
        Map<String, Object> syncmember = (Map<String, Object>) exchange.getProperty("syncmember");
        Map<String, Object> syncmemberacc = (Map<String, Object>) exchange.getProperty("syncmemberacc");
        Map<String, Object> lastreq = (Map<String, Object>) exchange.getProperty("lastreq");
        List<Map<String, Object>> cards = (List<Map<String, Object>>) memberProfile.get("membercards");
        List<Map<String, Object>> phones = (List<Map<String, Object>>) memberProfile.get("membercontacts");
        List<Map<String, Object>> addresses = (List<Map<String, Object>>) memberProfile.get("memberaddress");
        List<Map<String, Object>> tiers = (List<Map<String, Object>>) memberProfile.get("membertiers");
        List<Map<String, Object>> accounts = (List<Map<String, Object>>) memberProfile.get("memberaccount");
        String operation = "refresh";
        String request = "";
        if (lastreq != null && lastreq.get("request") != null) {
            request = (String) lastreq.get("request");
        }

        int seqnummember = 1;
        int seqnummemberacc = 1;

        log.info("REQ BEFORE:: " + request);


        String cardnumber = (String) exchange.getProperty("cardnumber");
        String accountid = (String) accounts.get(0).get("memberaccountid");

        String gender = "-";
        if (!StringUtils.isEmpty(memberProfile.get("gender"))) {
            if (memberProfile.get("gender").equals("FEMALE")) {
                gender = "Female";
            } else if (memberProfile.get("gender").equals("MALE")) {
                gender = "Male";
            } else {
                gender = "Male";
            }
        }

        String salutation = "";
        String nameprefix = "-";
        if (!StringUtils.isEmpty(memberProfile.get("salutationcode"))) {
            salutation = (String) memberProfile.get("salutationcode");

            if (salutation.equalsIgnoreCase("BAPAK") || salutation.equalsIgnoreCase("MR") || salutation.equalsIgnoreCase("MSTR")) {
                nameprefix = "Mr";
            } else if (salutation.equalsIgnoreCase("IBU") || salutation.equalsIgnoreCase("MRS")) {
                nameprefix = "Mrs";
            } else if (salutation.equalsIgnoreCase("MISS")) {
                nameprefix = "Miss";
            } else if (salutation.equalsIgnoreCase("MS")) {
                nameprefix = "Ms";
            } else {
                if (gender.equalsIgnoreCase("Female")) {
                    nameprefix = "Ms";
                } else {
                    nameprefix = "Mr";
                }
            }
        } else {
            if (gender.equalsIgnoreCase("Female")) {
                nameprefix = "Ms";
            } else {
                nameprefix = "Mr";
            }
        }

        String birthdate = "-";
        if (memberProfile.get("dateofbirth") != null) {
            birthdate = memberProfile.get("dateofbirth").toString();
        }

        String givenname = "-";
        String surname = "-";
        if (!StringUtils.isEmpty(memberProfile.get("firstname"))) {
            givenname = (String) memberProfile.get("firstname");
        }

        if (!StringUtils.isEmpty(memberProfile.get("lastname"))) {
            surname = (String) memberProfile.get("lastname");
        } else {
            surname = (String) memberProfile.get("firstname");
        }

        givenname = StringEscapeUtils.escapeXml10(givenname);
        surname = StringEscapeUtils.escapeXml10(surname);

        String telephone = "";
        String phoneloc = "";
        String phonetech = "";
        String phonenum = "";
        String phonetype = "";
        if (phones.size() == 1) {
            phonenum = (String) phones.get(0).get("phonenumber");
            phonetype = (String) phones.get(0).get("phonetype");
            if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("BUSINESSPHONE")) {
                phoneloc = "7";
            } else {
                phoneloc = "6";
            }

            if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("PRIVATEFAX")) {
                phonetech = "3";
            } else if (phonetype.equalsIgnoreCase("BUSINESSPHONE") || phonetype.equalsIgnoreCase("PRIVATEPHONE")) {
                phonetech = "1";
            } else {
                phonetech = "5";
            }
            telephone = "                     <Telephone PhoneLocationType=\"" + phoneloc + "\" PhoneTechType=\"" + phonetech + "\" PhoneNumber=\"" + phonenum + "\" DefaultInd=\"true\"/>\n";
        } else {
            for (Map<String, Object> phone : phones) {
                phonenum = (String) phone.get("phonenumber");
                phonetype = (String) phone.get("phonetype");

                if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("BUSINESSPHONE")) {
                    phoneloc = "7";
                } else {
                    phoneloc = "6";
                }

                if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("PRIVATEFAX")) {
                    phonetech = "3";
                } else if (phonetype.equalsIgnoreCase("BUSINESSPHONE") || phonetype.equalsIgnoreCase("PRIVATEPHONE")) {
                    phonetech = "1";
                } else {
                    phonetech = "5";
                }

                String preferred = "";
                if (phone.get("preferrednumber").equals(true)) {
                    telephone = "                     <Telephone PhoneLocationType=\"" + phoneloc + "\" PhoneTechType=\"" + phonetech + "\" PhoneNumber=\"" + phonenum + "\" DefaultInd=\"true\"/>\n";
                }

            }
        }

        String email = "-";
        if (!StringUtils.isEmpty(memberProfile.get("email"))) {
            email = (String) memberProfile.get("email");
        }


        String addressline = "None";
        String city = "Jakarta";
        String postalcode = "00000";
        String countrycode = "ID";
        String countryname = "Indonesia";

        String address = "";

        if (addresses.size() != 0) {
            for (Map<String, Object> memberaddress : addresses) {
                if (memberaddress.get("ispreffered").equals(true)) {
                    if (!StringUtils.isEmpty(memberaddress.get("address"))) {
                        addressline = (String) memberaddress.get("address");
                    }
                    city = (String) memberaddress.get("cityname");
                    if (!StringUtils.isEmpty(memberaddress.get("postalcode"))) {
                        postalcode = (String) memberaddress.get("postalcode");
                    }
                    countrycode = (String) memberaddress.get("countrycode");
                    countryname = (String) memberaddress.get("countryname");
                }
            }
        }

        addressline = StringEscapeUtils.escapeXml10(addressline);

        address = "                     <Address UseType=\"13\" DefaultInd=\"true\" Description=\"Home\" FormattedInd=\"true\">\n" +
                "                        <StreetNmbr PO_Box=\"00000\"/>\n" +
                "                        <AddressLine>" + addressline + "</AddressLine>\n" +
                "                        <CityName>" + city + "</CityName>\n" +
                "                        <PostalCode>" + postalcode + "</PostalCode>\n" +
                "                        <CountryName Code=\"" + countrycode + "\"/>\n" +
                "                     </Address>\n";


        String url = (String) exchange.getProperty("url");
        String env = (String) exchange.getProperty("env");

        String urlenv = url + "/" + env;

        //generate auth
        String messageID = GenerateVar.generate_messageID();
        String nonce = GenerateVar.generate_nonce();
        String created = GenerateVar.generate_created(exchange);
        String passwordAsli = "Garud@123";
        String passwdDigest = GenerateVar.password_digest(GenerateVar.password_encrypt("SHA-1", passwordAsli), nonce, created);
        String uniqueID = GenerateVar.generate_uniqueID();

        String passport = "00000";
        if (memberProfile.get("passportnumber") != null) {
            passport = (String) memberProfile.get("passportnumber");
        }

        String nameoncard = givenname + surname;
        if (memberProfile.get("nameoncard") != null) {
            nameoncard = (String) memberProfile.get("nameoncard");
        }

        retrocredit = retrocredit.concat("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\" xmlns:app=\"http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\" xmlns:typ1=\"http://xml.amadeus.com/2010/06/Types_v2\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlineUpdate_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n" +
                "      <AMA_UpdateRQ xmlns=\"http://xml.amadeus.com/2008/10/AMA/Profile\" xmlns:ns2=\"http://xml.amadeus.com/2010/06/Types_v2\" xmlns:ns3=\"http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd\" xmlns:ns4=\"http://www.opentravel.org/OTA/2003/05/OTA2010B\" xmlns:ns5=\"http://www.iata.org/IATA/2007/00/IATA2010.1\" Version=\"12.3\">\n" +
                "         <UniqueID Type=\"21\" ID_Context=\"LOYALTYNUMBER\" ID=\"" + cardnumber + "\"/>\n" +
                "         <ExternalID Type=\"1\" Instance=\"" + seqnummember + "\" ID_Context=\"ProfileRemoteID\" ID=\"" + cardnumber + "\"/>\n" +
                "         <ExternalID Type=\"1\" Instance=\"" + seqnummemberacc + "\" ID_Context=\"MileageRemoteID\" ID=\"" + cardnumber + "\"/>\n" +
                "         <Position XPath=\"//\">\n" +
                "            <Root Operation=\"" + operation + "\">\n" +
                "               <UniqueID Type=\"21\" ID_Context=\"LOYALTYNUMBER\" ID=\"" + cardnumber + "\"/>\n" +
                "               <UniqueID Type=\"9\" ID_Context=\"AIRCODE\" ID=\"GA\"/>\n" +
                "               <Profile ProfileType=\"21\">\n" +
                "                  <Customer Gender=\"" + gender + "\" BirthDate=\"" + birthdate + "\">\n" +
                "                     <PersonName>\n" +
                "                        <NamePrefix>" + nameprefix + "</NamePrefix>\n" +
                "                        <GivenName>" + givenname + "</GivenName>\n" +
                "                        <Surname>" + surname + "</Surname>\n" +
                "                     </PersonName>\n" +
                telephone +
                "                     <Email EmailType=\"1\">" + email + "</Email>\n" +
                address +
                "                     <CitizenCountryName Code=\"" + countrycode + "\"/>\n");

        String miles = "00000";
        if (accounts.get(0).get("awardmiles") != null) {
            double awardmiles = (double) accounts.get(0).get("awardmiles");
            miles = String.valueOf(awardmiles);
        }

        String valid = null;

        //tier
        String tierblue = (String) exchange.getProperty("blue");
        String tierciti = (String) exchange.getProperty("citi");
        String tiergold = (String) exchange.getProperty("gold");
        String tierplat = (String) exchange.getProperty("plat");
        String tierslvr = (String) exchange.getProperty("slvr");
        String tiertorb = (String) exchange.getProperty("torb");

        List<String> listtierblue = new ArrayList<>(Arrays.asList(tierblue.split(",")));
        List<String> listtierciti = new ArrayList<>(Arrays.asList(tierciti.split(",")));
        List<String> listtiergold = new ArrayList<>(Arrays.asList(tiergold.split(",")));
        List<String> listtierplat = new ArrayList<>(Arrays.asList(tierplat.split(",")));
        List<String> listtierslvr = new ArrayList<>(Arrays.asList(tierslvr.split(",")));
        List<String> listtiertorb = new ArrayList<>(Arrays.asList(tiertorb.split(",")));

        String tierid = (String) exchange.getProperty("tierid");
        String loyallevel = null;
        String leveldesc = "-";
        int counttier = 0;
        if (tiers.size() != 0) {
            for (String blue : listtierblue) {
                if (tierid.equals(blue)) {
                    loyallevel = "5";
                    leveldesc = "BLUE";
                    counttier += 1;
                }
            }
            for (String citi : listtierciti) {
                if (tierid.equals(citi)) {
                    loyallevel = "3";
                    leveldesc = "CITI";
                    counttier += 1;
                }
            }
            for (String gold : listtiergold) {
                if (tierid.equals(gold)) {
                    loyallevel = "2";
                    leveldesc = "GOLD";
                    counttier += 1;
                }
            }
            for (String plat : listtierplat) {
                if (tierid.equals(plat)) {
                    loyallevel = "1";
                    leveldesc = "PLAT";
                    counttier += 1;
                }
            }
            for (String slvr : listtierslvr) {
                if (tierid.equals(slvr)) {
                    loyallevel = "4";
                    leveldesc = "SLVR";
                    counttier += 1;
                }
            }
            for (String torb : listtiertorb) {
                if (tierid.equals(torb)) {
                    loyallevel = "1";
                    leveldesc = "TORB";
                    counttier += 1;
                }
            }
            if (counttier == 1) {
                retrocredit = retrocredit.concat("                     <CustLoyalty CustomerType=\"FREQ\" LoyalLevel=\"" + loyallevel + "\" LoyalLevelDescription=\"" + leveldesc + "\">\n" +
                        "                        <SubAccountBalance Type=\"Credit Miles\" Balance=\"" + miles + "\" RPH=\"TAT=SN:Mileage\"/>\n" +
                        "                     </CustLoyalty>\n");
            } else {
                log.info("Invalid Skyteam tier mapping");
                valid = "false";
                exchange.setProperty("valid", valid);
                return;
            }
        } else {
            log.info("Invalid Skyteam tier mapping");
            valid = "false";
            exchange.setProperty("valid", valid);
            return;
        }


        String langcode = "ID";
        if (!StringUtils.isEmpty(memberProfile.get("langcode"))) {
            langcode = (String) memberProfile.get("langcode");
        }

        retrocredit = retrocredit.concat("                     <LanguageSpoken Code=\"" + langcode + "\"/>\n" +
                "                  </Customer>\n" +
                "                  <PrefCollections>\n" +
                "                     <PrefCollection>\n" +
                "                        <AirlinePref/>\n" +
                "                     </PrefCollection>\n" +
                "                  </PrefCollections>\n" +
                "               </Profile>\n" +
                "            </Root>\n" +
                "         </Position>\n" +
                "      </AMA_UpdateRQ>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");


        Boolean samedata = false;
        int seqmemberafter = seqnummember + 1;
        int seqaccafter = seqnummemberacc + 1;
        String currentreq = retrocredit.replace("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\" xmlns:app=\"http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\" xmlns:typ1=\"http://xml.amadeus.com/2010/06/Types_v2\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlineUpdate_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n", "");
        log.info("CURRENT REQ::" + currentreq);
//        if (request != null && request.contains(currentreq)){
//            samedata = true;
//            exchange.setProperty("samedata", samedata);
//            return;
//        }else{
//            retrocredit = retrocredit.replace("Instance=\""+seqnummember+"\" ID_Context=\"ProfileRemoteID\"", "Instance=\"1\" ID_Context=\"ProfileRemoteID\"");
//            retrocredit = retrocredit.replace("Instance=\""+seqnummemberacc+"\" ID_Context=\"MileageRemoteID\"", "Instance=\"1\" ID_Context=\"MileageRemoteID\"");
//        }


        exchange.setProperty("reqxml", retrocredit);
        exchange.setProperty("samedata", samedata);
        exchange.setProperty("seqnummember", seqmemberafter);
        exchange.setProperty("seqnummemberacc", seqaccafter);
        String header = "http://webservices.amadeus.com/Profile_AirlineUpdate_12.3";
        getResultService(exchange, retrocredit, urlenv, header);
    }

    @SuppressWarnings("unchecked")
    public void setbodysoapUpdate(Exchange exchange) throws IOException, WSSecurityException, NoSuchAlgorithmException {
        String retrocredit = "";
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("res");
        Map<String, Object> syncmember = (Map<String, Object>) exchange.getProperty("syncmember");
        Map<String, Object> syncmemberacc = (Map<String, Object>) exchange.getProperty("syncmemberacc");
        Map<String, Object> lastreq = (Map<String, Object>) exchange.getProperty("lastreq");
        List<Map<String, Object>> cards = (List<Map<String, Object>>) memberProfile.get("membercards");
        List<Map<String, Object>> phones = (List<Map<String, Object>>) memberProfile.get("membercontacts");
        List<Map<String, Object>> addresses = (List<Map<String, Object>>) memberProfile.get("memberaddress");
        List<Map<String, Object>> tiers = (List<Map<String, Object>>) memberProfile.get("membertiers");
        List<Map<String, Object>> accounts = (List<Map<String, Object>>) memberProfile.get("memberaccount");
        String operation = (String) exchange.getProperty("operation");
        String request = "";
        if (lastreq != null && lastreq.get("request") != null) {
            request = (String) lastreq.get("request");
        }
        int seqnummember = 0;
        int seqnummemberacc = 0;
        //set seqnum by member
        if (syncmember != null) {
            if (syncmember.get("sequencenumber") != null) {
                seqnummember = (int) syncmember.get("sequencenumber");
            } else {
                seqnummember = 1;
            }
        } else {
            seqnummember = 1;
        }

        //set seqnum by account
        if (syncmemberacc != null) {
            if (syncmemberacc.get("sequencenumber") != null) {
                seqnummemberacc = (int) syncmemberacc.get("sequencenumber");
            } else {
                seqnummemberacc = 1;
            }
        } else {
            seqnummemberacc = 1;
        }

        log.info("REQ BEFORE:: " + request);

        String cardnumber = (String) exchange.getProperty("cardnumber");
        String accountid = (String) accounts.get(0).get("memberaccountid");

        String gender = "-";
        if (!StringUtils.isEmpty(memberProfile.get("gender"))) {
            if (memberProfile.get("gender").equals("FEMALE")) {
                gender = "Female";
            } else if (memberProfile.get("gender").equals("MALE")) {
                gender = "Male";
            } else {
                gender = "Male";
            }
        }

        String salutation = "";
        String nameprefix = "-";
        if (!StringUtils.isEmpty(memberProfile.get("salutationcode"))) {
            salutation = (String) memberProfile.get("salutationcode");

            if (salutation.equalsIgnoreCase("BAPAK") || salutation.equalsIgnoreCase("MR") || salutation.equalsIgnoreCase("MSTR")) {
                nameprefix = "Mr";
            } else if (salutation.equalsIgnoreCase("IBU") || salutation.equalsIgnoreCase("MRS")) {
                nameprefix = "Mrs";
            } else if (salutation.equalsIgnoreCase("MISS")) {
                nameprefix = "Miss";
            } else if (salutation.equalsIgnoreCase("MS")) {
                nameprefix = "Ms";
            } else {
                if (gender.equalsIgnoreCase("Female")) {
                    nameprefix = "Ms";
                } else {
                    nameprefix = "Mr";
                }
            }
        } else {
            if (gender.equalsIgnoreCase("Female")) {
                nameprefix = "Ms";
            } else {
                nameprefix = "Mr";
            }
        }

        String birthdate = "-";
        if (memberProfile.get("dateofbirth") != null) {
            birthdate = memberProfile.get("dateofbirth").toString();
        }

        String givenname = "-";
        String surname = "-";
        if (!StringUtils.isEmpty(memberProfile.get("firstname"))) {
            givenname = (String) memberProfile.get("firstname");
        }

        if (!StringUtils.isEmpty(memberProfile.get("lastname"))) {
            surname = (String) memberProfile.get("lastname");
        } else {
            surname = (String) memberProfile.get("firstname");
        }

        givenname = StringEscapeUtils.escapeXml10(givenname);
        surname = StringEscapeUtils.escapeXml10(surname);

//        String phonenum = "00000";
//        String phonenum2 = "00000";
//        if (phones.size() != 0) {
//            for (Map<String, Object> phone : phones) {
//                if (phone.get("phonetype").equals("MOBILE")) {
//                    phonenum = (String) phone.get("phonenumber");
//                } else {
//                    if (phone.get("phonetype").equals("PRIVATEPHONE")) {
//                        phonenum2 = (String) phone.get("phonenumber");
//                    } else {
//                        if (phone.get("phonetype").equals("BUSINESSPHONE")) {
//                            phonenum2 = (String) phone.get("phonenumber");
//                        }
//                    }
//                }
//            }
//        }

        String telephone = "";
        String phoneloc = "";
        String phonetech = "";
        String phonenum = "";
        String phonetype = "";

        if (phones.size() == 1) {
            phonenum = (String) phones.get(0).get("phonenumber");
            phonetype = (String) phones.get(0).get("phonetype");
            if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("BUSINESSPHONE")) {
                phoneloc = "7";
            } else {
                phoneloc = "6";
            }

            if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("PRIVATEFAX")) {
                phonetech = "3";
            } else if (phonetype.equalsIgnoreCase("BUSINESSPHONE") || phonetype.equalsIgnoreCase("PRIVATEPHONE")) {
                phonetech = "1";
            } else {
                phonetech = "5";
            }
            telephone = "                     <Telephone PhoneLocationType=\"" + phoneloc + "\" PhoneTechType=\"" + phonetech + "\" PhoneNumber=\"" + phonenum + "\" DefaultInd=\"true\"/>\n";
        } else {
            for (Map<String, Object> phone : phones) {
                phonenum = (String) phone.get("phonenumber");
                phonetype = (String) phone.get("phonetype");

                if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("BUSINESSPHONE")) {
                    phoneloc = "7";
                } else {
                    phoneloc = "6";
                }

                if (phonetype.equalsIgnoreCase("BUSINESSFAX") || phonetype.equalsIgnoreCase("PRIVATEFAX")) {
                    phonetech = "3";
                } else if (phonetype.equalsIgnoreCase("BUSINESSPHONE") || phonetype.equalsIgnoreCase("PRIVATEPHONE")) {
                    phonetech = "1";
                } else {
                    phonetech = "5";
                }

                String preferred = "";
                if (phone.get("preferrednumber").equals(true)) {
                    telephone = "                     <Telephone PhoneLocationType=\"" + phoneloc + "\" PhoneTechType=\"" + phonetech + "\" PhoneNumber=\"" + phonenum + "\" DefaultInd=\"true\"/>\n";
                }


            }
        }

        String email = "-";
        if (!StringUtils.isEmpty(memberProfile.get("email"))) {
            email = (String) memberProfile.get("email");
        }

        String addressline = "None";
        String city = "Jakarta";
        String postalcode = "00000";
        String countrycode = "ID";
        String countryname = "Indonesia";

        String address = "";

        if (addresses.size() != 0) {
            for (Map<String, Object> memberaddress : addresses) {
                if (memberaddress.get("ispreffered").equals(true)) {
                    if (!StringUtils.isEmpty(memberaddress.get("address"))) {
                        addressline = (String) memberaddress.get("address");
                    }
                    city = (String) memberaddress.get("cityname");
                    if (!StringUtils.isEmpty(memberaddress.get("postalcode"))) {
                        postalcode = (String) memberaddress.get("postalcode");
                    }
                    countrycode = (String) memberaddress.get("countrycode");
                    countryname = (String) memberaddress.get("countryname");
                }
            }
        }

        addressline = StringEscapeUtils.escapeXml10(addressline);

        address = "                     <Address UseType=\"13\" DefaultInd=\"true\" Description=\"Home\" FormattedInd=\"true\">\n" +
                "                        <StreetNmbr PO_Box=\"00000\"/>\n" +
                "                        <AddressLine>" + addressline + "</AddressLine>\n" +
                "                        <CityName>" + city + "</CityName>\n" +
                "                        <PostalCode>" + postalcode + "</PostalCode>\n" +
                "                        <CountryName Code=\"" + countrycode + "\"/>\n" +
                "                     </Address>\n";

        String url = (String) exchange.getProperty("url");
        String env = (String) exchange.getProperty("env");

        String urlenv = url + "/" + env;

        //generate auth
        String messageID = GenerateVar.generate_messageID();
        String nonce = GenerateVar.generate_nonce();
        String created = GenerateVar.generate_created(exchange);
        String passwordAsli = "Garud@123";
        String passwdDigest = GenerateVar.password_digest(GenerateVar.password_encrypt("SHA-1", passwordAsli), nonce, created);
        String uniqueID = GenerateVar.generate_uniqueID();

        String passport = "00000";
        if (memberProfile.get("passportnumber") != null) {
            passport = (String) memberProfile.get("passportnumber");
        }

        String nameoncard = givenname + surname;
        if (memberProfile.get("nameoncard") != null) {
            nameoncard = (String) memberProfile.get("nameoncard");
        }

        retrocredit = retrocredit.concat("\t<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlineUpdate_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n" +
                "      <AMA_UpdateRQ Version=\"12.3\" xmlns=\"http://xml.amadeus.com/2008/10/AMA/Profile\" xmlns:ns2=\"http://xml.amadeus.com/2010/06/Types_v2\" xmlns:ns3=\"http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd\" xmlns:ns4=\"http://www.opentravel.org/OTA/2003/05/OTA2010B\" xmlns:ns5=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "         <UniqueID Type=\"21\" ID_Context=\"LOYALTYNUMBER\" ID=\"" + cardnumber + "\"/>\n" +
                "         <ExternalID Type=\"1\" Instance=\"" + seqnummember + "\" ID_Context=\"ProfileRemoteID\" ID=\"" + cardnumber + "\"/>\n" +
                "         <Position XPath=\"//\">\n" +
                "            <Root Operation=\"" + operation + "\">\n" +
                "               <UniqueID Type=\"21\" ID_Context=\"LOYALTYNUMBER\" ID=\"" + cardnumber + "\"/>\n" +
                "               <UniqueID Type=\"9\" ID_Context=\"AIRCODE\" ID=\"GA\"/>\n" +
                "               <Profile ProfileType=\"21\">\n" +
                "                  <Customer Gender=\"" + gender + "\" BirthDate=\"" + birthdate + "\">\n" +
                "                     <PersonName>\n" +
                "                        <NamePrefix>" + nameprefix + "</NamePrefix>\n" +
                "                        <GivenName>" + givenname + "</GivenName>\n" +
                "                        <Surname>" + surname + "</Surname>\n" +
                "                     </PersonName>\n" +
                telephone +
                "                     <Email EmailType=\"1\">" + email + "</Email>\n" +
                address +
                "                     <CitizenCountryName Code=\"" + countrycode + "\"/>\n");

        String miles = "00000";
        if (accounts.get(0).get("awardmiles") != null) {
            double awardmiles = (double) accounts.get(0).get("awardmiles");
            miles = String.valueOf(awardmiles);
        }

        String valid = null;

        //tier
        String tierblue = (String) exchange.getProperty("blue");
        String tierciti = (String) exchange.getProperty("citi");
        String tiergold = (String) exchange.getProperty("gold");
        String tierplat = (String) exchange.getProperty("plat");
        String tierslvr = (String) exchange.getProperty("slvr");
        String tiertorb = (String) exchange.getProperty("torb");

        List<String> listtierblue = new ArrayList<>(Arrays.asList(tierblue.split(",")));
        List<String> listtierciti = new ArrayList<>(Arrays.asList(tierciti.split(",")));
        List<String> listtiergold = new ArrayList<>(Arrays.asList(tiergold.split(",")));
        List<String> listtierplat = new ArrayList<>(Arrays.asList(tierplat.split(",")));
        List<String> listtierslvr = new ArrayList<>(Arrays.asList(tierslvr.split(",")));
        List<String> listtiertorb = new ArrayList<>(Arrays.asList(tiertorb.split(",")));

        String tierid = (String) exchange.getProperty("tierid");
        String loyallevel = null;
        String leveldesc = "-";
        int counttier = 0;
        if (tiers.size() != 0) {
            for (String blue : listtierblue) {
                if (tierid.equals(blue)) {
                    loyallevel = "5";
                    leveldesc = "BLUE";
                    counttier += 1;
                }
            }
            for (String citi : listtierciti) {
                if (tierid.equals(citi)) {
                    loyallevel = "3";
                    leveldesc = "CITI";
                    counttier += 1;
                }
            }
            for (String gold : listtiergold) {
                if (tierid.equals(gold)) {
                    loyallevel = "2";
                    leveldesc = "GOLD";
                    counttier += 1;
                }
            }
            for (String plat : listtierplat) {
                if (tierid.equals(plat)) {
                    loyallevel = "1";
                    leveldesc = "PLAT";
                    counttier += 1;
                }
            }
            for (String slvr : listtierslvr) {
                if (tierid.equals(slvr)) {
                    loyallevel = "4";
                    leveldesc = "SLVR";
                    counttier += 1;
                }
            }
            for (String torb : listtiertorb) {
                if (tierid.equals(torb)) {
                    loyallevel = "1";
                    leveldesc = "TORB";
                    counttier += 1;
                }
            }
            if (counttier == 1) {
                retrocredit = retrocredit.concat("                     <CustLoyalty CustomerType=\"FREQ\" LoyalLevel=\"" + loyallevel + "\" LoyalLevelDescription=\"" + leveldesc + "\">\n" +
//                        "                        <SubAccountBalance Type=\"Credit Miles\" Balance=\"" + miles + "\" RPH=\"TAT=SN:Mileage\"/>\n" +
                        "                     </CustLoyalty>\n");
            } else {
                log.info("Invalid Skyteam tier mapping");
                valid = "false";
                exchange.setProperty("valid", valid);
                return;
            }
        } else {
            log.info("Invalid Skyteam tier mapping");
            valid = "false";
            exchange.setProperty("valid", valid);
            return;
        }


        String langcode = "ID";
        if (!StringUtils.isEmpty(memberProfile.get("langcode"))) {
            langcode = (String) memberProfile.get("langcode");
        }

        retrocredit = retrocredit.concat("                     <LanguageSpoken Code=\"" + langcode + "\"/>\n" +
                "                  </Customer>\n" +
                "                  <PrefCollections>\n" +
                "                     <PrefCollection>\n" +
                "                        <AirlinePref/>\n" +
                "                     </PrefCollection>\n" +
                "                  </PrefCollections>\n" +
                "               </Profile>\n" +
                "            </Root>\n" +
                "         </Position>\n" +
                "      </AMA_UpdateRQ>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");

        Boolean samedata = false;
        int seqmemberafter = seqnummember + 1;
        int seqaccafter = seqnummemberacc + 1;
        String currentreq = retrocredit.replace("\t<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlineUpdate_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n", "");
        log.info("CURRENT REQ::" + currentreq);
        if (request != null && request.contains(currentreq)) {
            samedata = true;
            exchange.setProperty("samedata", samedata);
            return;
        } else {
            retrocredit = retrocredit.replace("Instance=\"" + seqnummember + "\" ID_Context=\"ProfileRemoteID\"", "Instance=\"" + seqmemberafter + "\" ID_Context=\"ProfileRemoteID\"");
        }


        exchange.setProperty("reqxml", retrocredit);

        exchange.setProperty("samedata", samedata);
        exchange.setProperty("seqnummember", seqmemberafter);
        exchange.setProperty("seqnummemberacc", seqaccafter);
        String header = "http://webservices.amadeus.com/Profile_AirlineUpdate_12.3";
        getResultService(exchange, retrocredit, urlenv, header);
    }

    @SuppressWarnings("unchecked")
    public void setbodysoapPartialUpdate(Exchange exchange) throws IOException, WSSecurityException, NoSuchAlgorithmException {
        String retrocredit = "";
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("res");
        Map<String, Object> syncmember = (Map<String, Object>) exchange.getProperty("syncmember");
        Map<String, Object> syncmemberacc = (Map<String, Object>) exchange.getProperty("syncmemberacc");
        Map<String, Object> lastreq = (Map<String, Object>) exchange.getProperty("lastreq");
        List<Map<String, Object>> cards = (List<Map<String, Object>>) memberProfile.get("membercards");
        List<Map<String, Object>> accounts = (List<Map<String, Object>>) memberProfile.get("memberaccount");
        String operation = (String) exchange.getProperty("operation");
        String request = "";
        if (lastreq != null && lastreq.get("request") != null) {
            request = (String) lastreq.get("request");
        }
        int seqnummember = 0;
        int seqnummemberacc = 0;
        //set seqnum by member
        if (syncmember != null) {
            if (syncmember.get("sequencenumber") != null) {
                seqnummember = (int) syncmember.get("sequencenumber");
            } else {
                seqnummember = 1;
            }
        } else {
            seqnummember = 1;
        }

        //set seqnum by account
        if (syncmemberacc != null) {
            if (syncmemberacc.get("sequencenumber") != null) {
                seqnummemberacc = (int) syncmemberacc.get("sequencenumber");
            } else {
                seqnummemberacc = 1;
            }
        } else {
            seqnummemberacc = 1;
        }

        log.info("REQ BEFORE:: " + request);


        String cardnumber = (String) exchange.getProperty("cardnumber");

        String url = (String) exchange.getProperty("url");
        String env = (String) exchange.getProperty("env");

        String urlenv = url + "/" + env;

        //generate auth
        String messageID = GenerateVar.generate_messageID();
        String nonce = GenerateVar.generate_nonce();
        String created = GenerateVar.generate_created(exchange);
        String passwordAsli = "Garud@123";
        String passwdDigest = GenerateVar.password_digest(GenerateVar.password_encrypt("SHA-1", passwordAsli), nonce, created);
        String uniqueID = GenerateVar.generate_uniqueID();

        String miles = "00000";
        if (accounts.get(0).get("awardmiles") != null) {
            double awardmiles = (double) accounts.get(0).get("awardmiles");
            miles = String.valueOf(awardmiles);
        }

        retrocredit = retrocredit.concat("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlinePartialUpdate_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n" +
                "      <AMA_ProfilePartialUpdateRQ Version=\"12.3\" xmlns=\"http://xml.amadeus.com/2008/10/AMA/Profile\" xmlns:ns2=\"http://xml.amadeus.com/2010/06/Types_v2\" xmlns:ns3=\"http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd\" xmlns:ns4=\"http://www.opentravel.org/OTA/2003/05/OTA2010B\" xmlns:ns5=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "         <UniqueID Type=\"21\" ID_Context=\"LOYALTYNUMBER\" ID=\"" + cardnumber + "\"/>\n" +
                "         <UniqueID Type=\"9\" ID_Context=\"AIRCODE\" ID=\"GA\"/>\n" +
                "         <ExternalID Type=\"1\" Instance=\"" + seqnummember + "\" ID_Context=\"ProfileRemoteID\" ID=\"" + cardnumber + "\"/>\n" +
                "         <ExternalID Type=\"1\" Instance=\"" + seqnummemberacc + "\" ID_Context=\"MileageRemoteID\" ID=\"" + cardnumber + "\"/>\n" +
                "         <Position XPath=\"//\">\n" +
                "            <Root Operation=\"" + operation + "\">\n" +
                "               <Profile ProfileType=\"21\">\n" +
                "                  <Customer>\n" +
                "                     <CustLoyalty>\n" +
                "                        <SubAccountBalance Type=\"Credit Miles\" Balance=\"" + miles + "\" RPH=\"TAT=SN:Mileage\"/>\n" +
                "                     </CustLoyalty>\n" +
                "                  </Customer>\n" +
                "               </Profile>\n" +
                "            </Root>\n" +
                "         </Position>\n" +
                "      </AMA_ProfilePartialUpdateRQ>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");

        Boolean samedata = false;
        int seqmemberafter = seqnummember + 1;
        int seqaccafter = seqnummemberacc + 1;
        String currentreq = retrocredit.replace("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlinePartialUpdate_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n", "");
        log.info("CURRENT REQ::" + currentreq);
        if (request != null && request.contains(currentreq)) {
            samedata = true;
            exchange.setProperty("samedata", samedata);
            return;
        } else {
            retrocredit = retrocredit.replace("Instance=\"" + seqnummember + "\" ID_Context=\"ProfileRemoteID\"", "Instance=\"" + seqmemberafter + "\" ID_Context=\"ProfileRemoteID\"");
            retrocredit = retrocredit.replace("Instance=\"" + seqnummemberacc + "\" ID_Context=\"MileageRemoteID\"", "Instance=\"" + seqaccafter + "\" ID_Context=\"MileageRemoteID\"");
        }

        exchange.setProperty("reqxml", retrocredit);
        exchange.setProperty("samedata", samedata);
        exchange.setProperty("seqnummember", seqmemberafter);
        exchange.setProperty("seqnummemberacc", seqaccafter);
        String header = "http://webservices.amadeus.com/Profile_AirlinePartialUpdate_12.3";
        getResultService(exchange, retrocredit, urlenv, header);
    }

    @SuppressWarnings("unchecked")
    public void setbodysoapDelete(Exchange exchange) throws IOException, WSSecurityException, NoSuchAlgorithmException {
        String retrocredit = "";
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("res");
        List<Map<String, Object>> cards = (List<Map<String, Object>>) memberProfile.get("membercards");
        Map<String, Object> syncmember = (Map<String, Object>) exchange.getProperty("syncmember");
        Map<String, Object> syncmemberacc = (Map<String, Object>) exchange.getProperty("syncmemberacc");
        Map<String, Object> lastreq = (Map<String, Object>) exchange.getProperty("lastreq");
        String cardnumber = (String) exchange.getProperty("cardnumber");
        String request = "";
        if (lastreq != null && lastreq.get("request") != null) {
            request = (String) lastreq.get("request");
        }
        int seqnummember = 0;
        int seqnummemberacc = 0;
        //set seqnum by member
        if (syncmember != null) {
            if (syncmember.get("sequencenumber") != null) {
                seqnummember = (int) syncmember.get("sequencenumber");
            } else {
                seqnummember = 1;
            }
        } else {
            seqnummember = 1;
        }

        //set seqnum by account
        if (syncmemberacc != null) {
            if (syncmemberacc.get("sequencenumber") != null) {
                seqnummemberacc = (int) syncmemberacc.get("sequencenumber");
            } else {
                seqnummemberacc = 1;
            }
        } else {
            seqnummemberacc = 1;
        }

        log.info("REQ BEFORE:: " + request);


        String url = (String) exchange.getProperty("url");
        String env = (String) exchange.getProperty("env");

        String urlenv = url + "/" + env;

        //generate auth
        String messageID = GenerateVar.generate_messageID();
        String nonce = GenerateVar.generate_nonce();
        String created = GenerateVar.generate_created(exchange);
        String passwordAsli = "Garud@123";
        String passwdDigest = GenerateVar.password_digest(GenerateVar.password_encrypt("SHA-1", passwordAsli), nonce, created);
        String uniqueID = GenerateVar.generate_uniqueID();

        retrocredit = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlineDelete_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n" +
                "      <AMA_DeleteRQ Version=\"12.3\" xmlns=\"http://xml.amadeus.com/2008/10/AMA/Profile\" xmlns:ns2=\"http://xml.amadeus.com/2010/06/Types_v2\" xmlns:ns3=\"http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd\" xmlns:ns4=\"http://www.opentravel.org/OTA/2003/05/OTA2010B\" xmlns:ns5=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "         <UniqueID Type=\"21\" ID_Context=\"LOYALTYNUMBER\" ID=\"" + cardnumber + "\"/>\n" +
                "         <UniqueID Type=\"9\" ID_Context=\"AIRCODE\" ID=\"GA\"/>\n" +
                "         <DeleteRequests>\n" +
                "            <ProfileDeleteRequest ProfileType=\"21\"/>\n" +
                "         </DeleteRequests>\n" +
                "      </AMA_DeleteRQ>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";

        Boolean samedata = false;
        int seqmemberafter = seqnummember + 1;
        int seqaccafter = seqnummemberacc + 1;
        String currentreq = retrocredit.replace("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sec=\"http://xml.amadeus.com/2010/06/Security_v1\" xmlns:link=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:ses=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:prof=\"http://xml.amadeus.com/2008/10/AMA/Profile\">\n" +
                "   <soapenv:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "      <sec:AMA_SecurityHostedUser>\n" +
                "         <sec:UserID POS_Type=\"1\" RequestorType=\"U\" PseudoCityCode=\"JKTGA08LP\" AgentDutyCode=\"SU\">\n" +
                "            <typ:RequestorID xmlns:typ=\"http://xml.amadeus.com/2010/06/Types_v1\" xmlns:iat=\"http://www.iata.org/IATA/2007/00/IATA2010.1\">\n" +
                "               <iat:CompanyName>GA</iat:CompanyName>\n" +
                "            </typ:RequestorID>\n" +
                "         </sec:UserID>\n" +
                "      </sec:AMA_SecurityHostedUser>\n" +
                "      <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + messageID + "</add:MessageID>\n" +
                "      <add:Action xmlns:add=\"http://www.w3.org/2005/08/addressing\">http://webservices.amadeus.com/Profile_AirlineDelete_12.3</add:Action>\n" +
                "      <add:To xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + urlenv + "</add:To>\n" +
                "      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "         <wsse:UsernameToken>\n" +
                "            <wsse:Username>WSGALOY</wsse:Username>\n" +
                "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + passwdDigest + "</wsse:Password>\n" +
                "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonce + "</wsse:Nonce>\n" +
                "            <wsu:Created>" + created + "</wsu:Created>\n" +
                "         </wsse:UsernameToken>\n" +
                "      </wsse:Security>\n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n", "");
        log.info("CURRENT REQ::" + currentreq);
//        if (request != null && request.contains(currentreq)){
//            samedata = true;
//            exchange.setProperty("samedata", samedata);
//            return;
//        }


        exchange.setProperty("reqxml", retrocredit);
        exchange.setProperty("samedata", samedata);
        exchange.setProperty("seqnummember", seqmemberafter);
        exchange.setProperty("seqnummemberacc", seqaccafter);

        String header = "http://webservices.amadeus.com/Profile_AirlineDelete_12.3";
        getResultService(exchange, retrocredit, urlenv, header);
    }

    public void getMessage(Exchange exchange) {
        String resultxml = (String) exchange.getProperty("resultxml");

        String code = null;
        String status = null;

        Pattern psuccess = Pattern.compile(SUCCESS);
        Matcher msuccess = psuccess.matcher(resultxml);

        Pattern perror = Pattern.compile(ERROR);
        Matcher merror = perror.matcher(resultxml);

        Pattern psuccessupdate = Pattern.compile(SUCCESSUPDATE);
        Matcher msuccessupdate = psuccessupdate.matcher(resultxml);

        if (exchange.getProperty("operation").equals("createupdate") || exchange.getProperty("operation").equals("update")) {
            if (msuccessupdate.find()) {
                code = msuccessupdate.group(2);
                status = "success";
                log.info("PAS SUCCESS");
            } else if (merror.find()) {
                code = merror.group(2);
                status = "error";
                log.info("PAS ERROR");
            } else {
                code = "FAULT_CODE_ERROR";
                status = "othererror";
                log.info("PAS OTHER ERROR");
            }
        } else {
            if (msuccess.find()) {
                code = msuccess.group(1);
                status = "success";
                log.info("PAS SUCCESS");
            } else if (merror.find()) {
                code = merror.group(2);
                status = "error";
                log.info("PAS ERROR");
            } else {
                code = "FAULT_CODE_ERROR";
                status = "othererror";
                log.info("PAS OTHER ERROR");
            }
        }


        exchange.setProperty("code", code);
        exchange.setProperty("status", status);
    }

    public void getResultService(Exchange exchange, String retrocredit, String urlenv, String header) throws IOException {
//        System.setProperty("java.net.useSystemProxies", "true");
        log.info("MASUK HIT SERVIS");
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(interceptor).build();
        MediaType mediaType = MediaType.parse("application/xml");
        RequestBody body = RequestBody.create(mediaType, retrocredit);

        Request request = new Request.Builder()
                .url(urlenv)
                .method("POST", body)
                .addHeader("Content-Type", "application/xml")
                .addHeader("SoapAction", header)
                .build();
        Response response = client.newCall(request).execute();
        String result = response.body().string();
        exchange.setProperty("resultxml", result);
    }

}
